#
#
#
#


import pathlib,re

dapLinkDETAILSTXTre=re.compile("""(?P<attr>[^:]+):(?P<value>.*)""")

class microbit():
    def __init__(self):
        self.detailsTxtAttr=dict()
        self.CNBANoffset=19

    def CNBANget(self):
        self.CNBANoffset = 19
        result=None
        if len(self.detailsTxtAttr.keys()) > 0:
            if "Unique ID" in self.detailsTxtAttr.keys():
                result="21"+self.detailsTxtAttr["Unique ID"][self.CNBANoffset:self.CNBANoffset+2] #TODO: prefijo debe ser programable
            else:
                print(f"WARN: microbit no tiene atributo Unique ID")
        else:
            print(f"WARN: microbit no tiene atributos, ignorado")
        return result

    def detailsTxtCargar(self, dapLinkPath="",
                 dapLinkDETAILSTXT="DETAILS.TXT"):
        fin_name = pathlib.Path(dapLinkPath) / dapLinkDETAILSTXT
        try:
            with open(str(fin_name)) as fin:
                idx=0
                line = fin.readline()
                while line != "":
                    line = fin.readline()
                    if len(line) > 0 and line[0] != "#":
                        try:
                            reResult=dapLinkDETAILSTXTre.search(line)
                            self.detailsTxtAttr[reResult.groups()[0]]=reResult.groups()[1]
                        except Exception as e:
                            print(f"DEBUG: línea (no comentario) {idx}ignorada: {line}, excepcion: {e}")
                    idx+=1
        except Exception as e:
            print(f"ERROR: no puedo cargar DETAILS.TXT, error {str(e)}")