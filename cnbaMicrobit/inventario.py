import pathlib, pickle

from cnbaMicrobit import microbit

def microbitCargarLoteHaciaPickle(variosMicrobit=list()):
    while input("cargar otro microbit? [ENTER para terminar]") != "":
        unMibrobit=microbit()
        unMibrobit.detailsTxtCargar(dapLinkPath="/Volumes/MICROBIT",dapLinkDETAILSTXT="DETAILS.TXT")
        if len(unMibrobit.detailsTxtAttr.keys()) > 0:
            if "Unique ID" in unMibrobit.detailsTxtAttr.keys():
                unMibrobit.CNBAN=unMibrobit.CNBANget()
                print(f"INFO: microbit cargado, UniqueID={unMibrobit.detailsTxtAttr['Unique ID']}, CNBAN = {unMibrobit.CNBAN}")
            else:
                print(f"WARN: microbit cargado, pero no tiene atributo Unique ID")
            if True: #TODO: verificar duplicados... :(
                variosMicrobit.append(unMibrobit)
        else:
            print(f"WARN: microbit no tiene atributos, ignorado")
    pass
    with open(str(pathlib.Path.home() / "variosMicrobit.pickle"),mode="wb") as fout:
        pp=pickle.Pickler(fout)
        pp.dump(variosMicrobit)
        print(f"INFO: variosMicrobit, total {len(variosMicrobit)}, guardados en {fout.name}")

def microbitCargarLoteDesdePickle(variosMibrobit=list(),file_name="variosMicrobit.pickle"):
    lenAntes=len(variosMibrobit)
    with open(str(pathlib.Path.home() / file_name),mode="rb") as fin:
        pp=pickle.Unpickler(fin)
        variosMibrobit+=pp.load()
        print(f"variosMicrobit, total {len(variosMibrobit)-lenAntes}, cargados {len(variosMibrobit)} desde {fin.name}")



if __name__ == '__main__':
    variosMibrobit = list()
    #
    # Cargar inventario desde pickle
    #
    microbitCargarLoteDesdePickle(variosMibrobit, "variosMicrobit.pickle")
    for unMicrobit in variosMibrobit:
        if len(unMicrobit.detailsTxtAttr.keys()) > 0:
            print(f"Nro de registro: {unMicrobit.CNBAN}, Unique ID: {unMicrobit.detailsTxtAttr['Unique ID'][:5]}*****************************")
        else:
            print(f"WARN: elemento sin atributos... {unMicrobit}")
    #
    # Actualizar inventario con nuevas placas y salvar en pickle
    #
    microbitCargarLoteHaciaPickle(variosMibrobit)
    print()
