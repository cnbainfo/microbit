# cnbaMicrobit

## Intro

Biblioteca para gestión de [BBC micro:bit](http://makecode.microbit.org/) y aplicación didáctica  en el [Colegio Nacional de Buenos Aires](https://www.cnba.uba.ar/academico/departamentos/informatica)

## Casos de uso

### Inventario

Inventariar varias placas conectandolas al puerto USB (se ven como un pendrive con los drivers daplink instalados) 


#### Guardado en un archivo pickle


```bash
/usr/bin/python3 /Users/lb/cnbaMicrobit/inventario.py

variosMicrobit, total 19, cargados 19 desde /Users/lb/variosMicrobit_todas.pickle
Nro de registro: 2181, Unique ID:  9905*****************************
Nro de registro: 2167, Unique ID:  9905*****************************
Nro de registro: 21c7, Unique ID:  9905*****************************
Nro de registro: 2132, Unique ID:  9905*****************************
Nro de registro: 21ba, Unique ID:  9905*****************************
Nro de registro: 21df, Unique ID:  9905*****************************
Nro de registro: 2160, Unique ID:  9905*****************************
Nro de registro: 210b, Unique ID:  9905*****************************
Nro de registro: 21f3, Unique ID:  9905*****************************
Nro de registro: 21dd, Unique ID:  9905*****************************
Nro de registro: 21cc, Unique ID:  9905*****************************
Nro de registro: 2121, Unique ID:  9905*****************************
Nro de registro: 21d9, Unique ID:  9905*****************************
Nro de registro: 21cf, Unique ID:  9905*****************************
Nro de registro: 21a1, Unique ID:  9905*****************************
Nro de registro: 2140, Unique ID:  9905*****************************
Nro de registro: 2148, Unique ID:  9905*****************************
Nro de registro: 217e, Unique ID:  9905*****************************
Nro de registro: 21ac, Unique ID:  9905*****************************
cargar otro microbit? [ENTER para terminar]si
INFO: microbit cargado, UniqueID= 9905******************************, CNBAN = 21c3
cargar otro microbit? [ENTER para terminar]
variosMicrobit, total 20, guardados en /Users/lb/variosMicrobit.pickle
```

## Autores

- Leandro Batlle <info.arroba.cnba.uba.ar@gmail.com>

## Licencia

GPL v3